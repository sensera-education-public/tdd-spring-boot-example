package com.example.tddspringbootexample;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class NamesControllerTest {

    @Autowired NamesController namesController;

    @Autowired NamesList namesList;

    @Autowired NamesService namesService;

    @BeforeEach
    void setUp() {
        namesService.addName("Arne","Anka");
        namesService.addName("Agda","Anka");
        namesService.addName("Beda","Anka");
    }

    @AfterEach
    void tearDown() {
        namesList.clearNames();
    }

    @Test
    void test_get_all_surenames_success() {
        List<String> names = namesController.getAllSurenames();

        assertEquals(List.of("Arne","Agda","Beda"), names);
    }

    @Test
    void test_get_all_lastnames_success() {
        List<String> names = namesController.getAllLastames();

        assertEquals(List.of("Anka","Anka","Anka"), names);
    }
}
