package com.example.tddspringbootexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TddSpringBootExampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(TddSpringBootExampleApplication.class, args);
    }

}
