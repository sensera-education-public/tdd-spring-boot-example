package com.example.tddspringbootexample;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class NamesService {

    @Autowired NamesList namesList;

    public List<Name> allNames() {
        return namesList.all();
    }

    public Name addName(String surname, String lastname) {
        Name name = new Name(surname, lastname);
        namesList.add(name);
        return name;
    }


}
