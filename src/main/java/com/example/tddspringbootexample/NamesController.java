package com.example.tddspringbootexample;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class NamesController {

    @Autowired NamesService namesService;

    @GetMapping("/surenames")
    public List<String> getAllSurenames() {
        return namesService.allNames().stream()
                .map(name -> name.getSurename())
                .collect(Collectors.toList());
    }

    @GetMapping("/lastnames")
    public List<String> getAllLastames() {
        return namesService.allNames().stream()
                .map(name -> name.getLastname())
                .collect(Collectors.toList());
    }
}
