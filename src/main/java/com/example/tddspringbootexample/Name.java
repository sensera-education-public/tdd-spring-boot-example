package com.example.tddspringbootexample;

import lombok.Value;

import java.util.Objects;

@Value
public class Name {
    String surename;
    String lastname;
}
