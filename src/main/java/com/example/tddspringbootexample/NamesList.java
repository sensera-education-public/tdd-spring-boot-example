package com.example.tddspringbootexample;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class NamesList {
    List<Name> names = new ArrayList<>();

    public void clearNames() {
        names.clear();
    }

    public List<Name> all() {
        return names;
    }

    public void add(Name name) {
        names.add(name);
    }
}
